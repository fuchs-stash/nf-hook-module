# obj-m += hello1.o
# obj-m += tcp_custom.o
obj-m += nf_hook.o
obj-m += in_nf_hook.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
