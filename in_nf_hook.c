#include <asm-generic/fcntl.h>
#include <linux/in.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/module.h>

#include <linux/atomic.h>
#include <linux/ip.h>
#include <linux/net.h>
#include <linux/netfilter.h>
#include <linux/socket.h>
#include <linux/tcp.h>

static int custom_hook_init(void);
static void custom_hook_exit(void);
void compute_tcp_checksum(struct iphdr *pIph, u16 *ipPayload);

module_init(custom_hook_init);
module_exit(custom_hook_exit);
MODULE_LICENSE("GPL");

#define TEST_RWND_STEPS 0
#define FORCE_LOSS_ENABLED 1
#define DISABLE_ALL 0

#define own_tcp_sk(ptr)                                                        \
  _Generic(ptr,                                                                \
      const typeof(*(ptr)) *: ((const struct tcp_sock *)container_of(          \
          ptr, struct tcp_sock, inet_conn.icsk_inet.sk)),                      \
      default: ((struct tcp_sock *)container_of(ptr, struct tcp_sock,          \
                                                inet_conn.icsk_inet.sk)))

#define MS_TO_NS(x) ((x) * 1000000)

// Used to communicate the BW changes from the TCP-listener Thread to the
// callback. This could also be done by using the priv data, that is passed to
// the callback
struct private_data {
  struct socket *control_sock;
  struct sockaddr_in *info;
  atomic_t new_bw;
};
struct private_data *priv_data = NULL;
struct task_struct *thread_task = NULL;

/**
 * Stores the port of iperf3 in big endian (set in main)
 */
static __be16 iperf3_port;

/*
 * Called for every outgoing packet
 * Behaviour defined in main at hook-registration
 */
static unsigned int custom_hook(void *priv, struct sk_buff *skb,
                                const struct nf_hook_state *state) {

#if DISABLE_ALL
  // Early return to disable module
  return NF_ACCEPT;
#endif

  /*
  Early ACCEPT for everything that does not interest us
  */
  if (IS_ERR_OR_NULL(skb))
    return NF_DROP;

  struct iphdr *iphdr = ip_hdr(skb);

  if (iphdr->protocol != IPPROTO_TCP)
    return NF_ACCEPT;

  struct tcphdr *tcphdr = tcp_hdr(skb);

  if (tcphdr->source != iperf3_port)
    return NF_ACCEPT;

  /*
  The Idea here is that right after the bw drop, a certain ammount of loss is
  introduced. This is done by dropping a coule of incomming packets. This is
  done in order to reduce the cwnd of the server.
  */

  // Updated every time a bw change is detected
  static int local_new_bw = 0;
  static int local_old_bw = 0;

  // Whether there is currently an artificial packet drop introduced
  static bool loss_introduced_state = false;

  // The time a bw reduction was recorded/received
  static struct timespec64 loss_introduced_timepoint;

/*
0: Drop percentage over time
1: Drop N packets on bw-loss
2: Drop N packets on bw-loss, then drop a M packets every X seconds
*/
#define DROP_MODE 2

  /*
  For Drop-Mode 0
  */
  // How long the artificial drop should last (Assuming the bw doesnt return to
  // 'normal' in this time) [s]
#define LOSS_HOLD_TIME 1

  // The percentage of packets that should be dropped
#define LOSS_PERCENTAGE_ZE_DD 50

/*
For Drop-Mode 1
*/
#define NUM_PACKETS_TO_LOSE 9

/*
For Drop-Mode 2
*/
#define MODE_2_LOSS_HOLD_TIME 10
#define INITIAL_DROP_AMMOUNT 1
#define DROP_INTERVAL_S 1
#define DROP_INTERVAL_MS 0
#define DROP_PER_INTERVAL 1
  static bool initial_drop_over = false;
  static int packets_dropped_this_interval = 0;
  static bool in_drop_interval = false;

  //

  static int dropped_packets = 0;
  static int packet_counter = 0;

  //   static struct timespec64 loss_begin_time = {.tv_nsec = 0, .tv_sec = 0};

  // Initialization, because static vars need to be const-initialized
  static bool initialized = false;
  if (!initialized) {
    // atomic_set(&bw, 0); // Apparently this callback is not parralelized
    ktime_get_ts64(&loss_introduced_timepoint);
    initialized = true;
  }

/*
 * The main Functionality of this Module. Reducde rwnd if a bw drop is
 * detected
 */
#if FORCE_LOSS_ENABLED

  int new_bw = atomic_read(&priv_data->new_bw);

  if (local_new_bw != new_bw) {
    local_old_bw = local_new_bw;
    local_new_bw = new_bw;
    pr_info("New BW: %d; \tOld BW: %d\n", local_new_bw, local_old_bw);

    if (local_new_bw < 100) {
      loss_introduced_state = true;
      dropped_packets = 0;
      packet_counter = 0;

      // For Mode 3
      initial_drop_over = false;
      in_drop_interval = false;
      packets_dropped_this_interval = 0;

      ktime_get_ts64(&loss_introduced_timepoint);

    } else {
      loss_introduced_state = false;
    }
  }

  if (loss_introduced_state) {
    struct timespec64 curr_time;
    ktime_get_ts64(&curr_time);

    time64_t delta_s_since_start =
        curr_time.tv_sec - loss_introduced_timepoint.tv_sec;

    struct timespec64 delta =
        timespec64_sub(curr_time, loss_introduced_timepoint);

    packet_counter += 1;

#if DROP_MODE == 0
    if ((10000 / LOSS_PERCENTAGE_ZE_DD) * dropped_packets < packet_counter) {
      dropped_packets += 1;
      return NF_DROP;
    }

    if (delta_s_since_start > LOSS_HOLD_TIME) {
      loss_introduced_state = false;
    }

#elif DROP_MODE == 1

    if (dropped_packets < NUM_PACKETS_TO_LOSE) {
      dropped_packets += 1;
      return NF_DROP;
    }
    if (delta_s_since_start > LOSS_HOLD_TIME) {
      loss_introduced_state = false;
    }
#else

    static struct timespec64 last_drop_timepoint;
    if (!initial_drop_over) {

      if (dropped_packets < INITIAL_DROP_AMMOUNT) {
        dropped_packets += 1;
        ktime_get_ts64(&last_drop_timepoint);
        return NF_DROP;
      } else {
        initial_drop_over = true;
      }
    }

    if (initial_drop_over) {
      struct timespec64 diff = timespec64_sub(curr_time, last_drop_timepoint);

      if (diff.tv_nsec >= MS_TO_NS(DROP_INTERVAL_MS) &&
          diff.tv_sec >= DROP_INTERVAL_S) {
        in_drop_interval = true;
        ktime_get_ts64(&last_drop_timepoint);
        packets_dropped_this_interval = 0;
        // ktime_get_ts64(&last_drop_timepoint);
      }

      if (in_drop_interval) {
        if (packets_dropped_this_interval < DROP_PER_INTERVAL) {
          packets_dropped_this_interval += 1;

          return NF_DROP;
        }

        if (packets_dropped_this_interval >= DROP_PER_INTERVAL) {
          //   packets_dropped_this_interval = 0;
          //   ktime_get_ts64(&last_drop_timepoint);
          in_drop_interval = false;
        }
      }
    }

    if (delta_s_since_start > MODE_2_LOSS_HOLD_TIME) {
      loss_introduced_state = false;
    }
#endif
  }
#endif

/*
 * Test Functionality for slowly increasing the rwnd
 * This was used to evaluate different rwnd values
 */
#if TEST_RWND_STEPS
  static bool initialized_2 = false;
  static struct timespec64 start_time;
  if (!initialized_2) {
    ktime_get_ts64(&start_time);
    initialized_2 = true;
  }

#define START_RWND 400
  static const u16 stages[] = {START_RWND, 400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                               0,          0,   0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  struct timespec64 curr_time;
  ktime_get_ts64(&curr_time);
  static int ctr = 0;
  static __be16 wnd = htons(START_RWND * SCALING);
  static bool use_default_window = false;
  if (curr_time.tv_sec - start_time.tv_sec > 20) {
    ctr += 1;
    pr_info("20 seconds have passed\n");
    start_time = curr_time;
    use_default_window = (stages[(ctr % 30)] == 0);
    wnd = htons(stages[(ctr % 20)] * SCALING);
  }
  if (!use_default_window) {
    tcphdr->window = wnd;
  }

#endif

  // Do i even need to manually compute the checksum?
  // compute_tcp_checksum(iphdr, (u16 *)tcphdr);

  return NF_ACCEPT;
}

static int listener_thread(void *data) {

  /*
    Format: <OLD_BW(size:3)>|<NEW_BW(size:3)>|
    If a there is no delimiter is pos 3 and 7 something has gone wrong
  */
  const size_t FIRST_DELIM_POS = 3;
  const size_t SECOND_DELIM_POS = 7;

  const size_t buf_size = 48;
  char *buf = (char *)kcalloc(buf_size + 1, sizeof(char), GFP_KERNEL);

  struct kvec iov = {buf, buf_size};
  struct msghdr msg = {.msg_name = NULL, .msg_flags = 0};

  if (IS_ERR_OR_NULL(priv_data->control_sock))
    return 1;

  if (priv_data->control_sock->state != SS_CONNECTED) {
    pr_err("Socket not connected\n");
    return 1;
  }

  while (!kthread_should_stop()) {

    msleep(500);

    // Clear relevant portion of buffer
    memset(buf, 0, 7);

    int num_bytes = kernel_recvmsg(priv_data->control_sock, &msg, &iov, 1,
                                   buf_size, msg.msg_flags);

    pr_info("Received %d bytes (msg: |%s|)\n", num_bytes, buf);

    if (num_bytes < 0) {
      pr_warn("Err Code: %d (%pe)\n", num_bytes, ERR_PTR(num_bytes));
      continue;
    }

    if (buf[FIRST_DELIM_POS] != '|' || buf[SECOND_DELIM_POS] != '|') {
      pr_warn("Delimiter not found in expected positions!\n");
      continue;
    }

    buf[FIRST_DELIM_POS] = '\0';
    int bw = 0;
    int ret = sscanf(buf, "%d", &bw);
    if (ret != 1) {
      pr_warn("Could not convert buf to number (buf: |%s|) (Err: %d; %pe)\n",
              buf, ret, ERR_PTR(ret));
      continue;
    }

    pr_info("Read %d\n", bw);
    atomic_set(&priv_data->new_bw, bw);
  }

  return 0;
}

static void start_listening_thread(void) {
  thread_task = kthread_run(listener_thread, NULL, "listener");
  if (IS_ERR(thread_task)) {
    printk(KERN_ERR "Failed to create listening thread\n");
  }
}

static struct net *net_ns;

static struct nf_hook_ops hook_ops = {
    .hook = custom_hook,
    .pf = NFPROTO_IPV4,
    .hooknum = NF_INET_LOCAL_IN, //  All packets that are going to a local
                                 //  process (e.g. all incomming packets)
    .priority = 0,               // can be set up to -200?
    .priv = NULL,                // Can be set to include private data
};

static int custom_hook_init(void) {

  pr_info("Registering custom module NF-HOOK...\n");
  iperf3_port = htons(5201);
  pr_info("Using iperf3 port: %d\n", iperf3_port);

  /* Checkout nsproxy.h -> can be dereferenced without precautions */
  net_ns = current->nsproxy->net_ns;
  if (!net_ns) {
    pr_err("No net_namespace could be found for current\n");
    return -1;
  }

  priv_data = kmalloc(sizeof(struct private_data), GFP_KERNEL);
  if (!priv_data) {
    pr_err("KMalloc Failed!\n");
    return -1;
  }

  // Create socket
  struct socket *sock = kmalloc(sizeof(struct socket), GFP_KERNEL);
  int err =
      sock_create_kern(&init_net, AF_INET, SOCK_STREAM, IPPROTO_TCP, &sock);
  if (err < 0) {
    printk(KERN_ERR "Failed to create socket (%d)\n", err);
    return -1;
  }

  // Connect socket
  {
    struct sockaddr_in *addr = kmalloc(sizeof(struct sockaddr_in), GFP_KERNEL);
    addr->sin_family = AF_INET, addr->sin_port = htons(9998);
    addr->sin_addr = (struct in_addr){.s_addr = htonl(INADDR_LOOPBACK)};
    priv_data->info = addr;
    int ret = kernel_connect(sock, (struct sockaddr *)addr,
                             sizeof(struct sockaddr), O_RDWR);
    if (ret != 0) {
      printk(KERN_ERR "Failed to connect to socket (%d)\n", ret);
      return -1;
    }
  }

  priv_data->control_sock = sock;
  priv_data->new_bw = (atomic_t)ATOMIC_INIT(0);

  hook_ops.priv = (void *)priv_data;

  start_listening_thread();

  return nf_register_net_hook(&init_net, &hook_ops);
}

static void custom_hook_exit(void) {
  pr_info("Unregistering custom module NF-HOOK...\n");
  if (thread_task != NULL) {
    kthread_stop(thread_task);
    msleep(1250);
  }

  priv_data->control_sock->ops->shutdown(priv_data->control_sock, SHUT_RDWR);
  priv_data->control_sock->ops->release(priv_data->control_sock);

  kfree(priv_data->info);
  //   kfree(priv_data->data_sock);
  kfree(priv_data->control_sock);
  kfree(priv_data);

  nf_unregister_net_hook(&init_net, &hook_ops);
}
