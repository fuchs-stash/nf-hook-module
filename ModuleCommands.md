
Insert Module: 
	sudo insmod ./tcp_custom.ko

Unload Module with dependencies: 
	((Evtl nicht nötig, wenn man mit -C cc bei iperf3 setzt)) sudo sysctl net.ipv4.tcp_congestion_control=bbr 
	sudo cp tcp_custom.ko /lib/modules/`uname -r`
	sudo depmod -a
	modprobe --remove-dependencies -f -r tcp_custom
	lsmod | grep tcp 
		-> Sollte auf 0 sein, sonst sachen wie browser, ... schließen 
