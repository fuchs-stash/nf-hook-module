#include <asm-generic/fcntl.h>
#include <linux/in.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/module.h>

#include <linux/atomic.h>
#include <linux/ip.h>
#include <linux/net.h>
#include <linux/netfilter.h>
#include <linux/socket.h>
#include <linux/tcp.h>

static int custom_hook_init(void);
static void custom_hook_exit(void);
void compute_tcp_checksum(struct iphdr *pIph, u16 *ipPayload);

module_init(custom_hook_init);
module_exit(custom_hook_exit);
MODULE_LICENSE("GPL");

#define TEST_RWND_STEPS 0
#define FORCE_RWND_ENABLED 1
#define DISABLE_ALL 0

#define own_tcp_sk(ptr)                                                        \
  _Generic(ptr,                                                                \
      const typeof(*(ptr)) *: ((const struct tcp_sock *)container_of(          \
          ptr, struct tcp_sock, inet_conn.icsk_inet.sk)),                      \
      default: ((struct tcp_sock *)container_of(ptr, struct tcp_sock,          \
                                                inet_conn.icsk_inet.sk)))

// the LUT has a step size of 10 MHz.
// The first entry is for 0 MHz, the second for 10 MHz, etc.
// The values contained are the bandwidth-delay-product, which means that these
// are the max values for the rwnd. So once a value is used, start with a
// fraction of this value and the slowly work up to the max value
// Entries with the value 0 mean that there is no restriction for this bandwidth
// *** Test with bandwidth-delay-product values for a delay of ~150ms ***
u16 bw_rwnd_lut[] = {
    200, 200, 200, 400, 400, 400, 400, 500, 0, 0, //
    0,   0,   0,   0,   0,   0,   0,   0,   0,    //
    0,   0,   0,   0,   0,   0,   0,   0,   0     //
};
#define LUT_VAL(bw) (bw_rwnd_lut[(bw) / 10])
#define MIN_RWND_VAL 75

#define MS_TO_NS(x) ((x) * 1000000)

// Used for scaling a wanted rwnd to the value that has to be
// written to the tcp-window-field. Assumes mss=1460 and
// scaling-factor=128 (e.g. 1460 / 128 ~= 11.40625); Abuses the
// macro-copy-paste functionality for avoiding floating point arithmetic (e.g.
// first mult with 115, the divide by 10)
#define SCALING 115 / 10

// Used to communicate the BW changes from the TCP-listener Thread to the
// callback. This could also be done by using the priv data, that is passed to
// the callback
struct private_data {
  struct socket *control_sock;
  struct sockaddr_in *info;
  atomic_t new_bw;
};
struct private_data *priv_data = NULL;
struct task_struct *thread_task = NULL;

/**
 * Stores the port of iperf3 in big endian (set in main)
 */
static __be16 iperf3_port;

/*
 * Called for every outgoing packet
 * Behaviour defined in main at hook-registration
 */
static unsigned int custom_hook(void *priv, struct sk_buff *skb,
                                const struct nf_hook_state *state) {

#if DISABLE_ALL
  // Early return to disable module
  return NF_ACCEPT;
#endif

  /*
  Early ACCEPT for everything that does not interest us
  */
  if (IS_ERR_OR_NULL(skb))
    return NF_DROP;

  struct iphdr *iphdr = ip_hdr(skb);

  if (iphdr->protocol != IPPROTO_TCP)
    return NF_ACCEPT;

  struct tcphdr *tcphdr = tcp_hdr(skb);

  if (tcphdr->dest != iperf3_port)
    return NF_ACCEPT;

  /*
  The Idea here is that right after the bw drop, the window is reduced quite a
  bit and then slowly rises to the value in the LUT. This is to try and drain
  the queue which has probably accumulated due to the bw drop.
  */

  // Updated every time a bw change is detected
  static int local_new_bw = 0;
  static int local_old_bw = 0;

  // The current window size
  static u16 window = 0;
  static u16 window_unscaled = 0;

  // Whether the rwnd is currently manually reduced
  static bool bw_reduced_state = false;

  // The time a bw reduction was recorded/received
  static struct timespec64 bw_reduced_timer;

  // How long the window should be reduced (Assuming the bw doesnt return to
  // 'normal' in this time) [s]
#define RWND_HOLD_TIME 20

// How long it should take for the rwnd to reach the max value [s]
#define RWND_GROW_TIME 5

// The factor by which the Bandwidth-Delay-Product is divided when a bw drop is
// detected. This results in the starting value for the rwnd
#define RWND_START_REDUCTION_FACTOR 5

  // How much the rwnd should be increased every 100ms
  // default val; Updated when every time a bw drop is detected
  static u16 rwnd_per_100_ms = 1;

  // Set to the value from the LUT
  static u16 curr_max_rwnd = U16_MAX;
  static struct timespec64 last_rwnd_incr_time = {.tv_nsec = 0, .tv_sec = 0};

  // Initialization, because static vars need to be const-initialized
  static bool initialized = false;
  if (!initialized) {
    // atomic_set(&bw, 0); // Apparently this callback is not parralelized
    ktime_get_ts64(&bw_reduced_timer);
    initialized = true;
  }

  // Way of getting the cwnd (tested, works); not needed right now
  //   struct tcp_sock *tcp_sk = own_tcp_sk(state->sk); // tcp_sk(state->sk);
  //   pr_info("%d\n", tcp_sk->rcvq_space.space);
  //   u32 cwnd = tcp_sk->snd_cwnd;

/*
 * The main Functionality of this Module. Reducde rwnd if a bw drop is
 * detected
 */
#if FORCE_RWND_ENABLED

  int new_bw = atomic_read(&priv_data->new_bw);

  if (local_new_bw != new_bw) {
    local_old_bw = local_new_bw;
    local_new_bw = new_bw;
    // atomic_set(&bw, new_bw);
    pr_info("New BW: %d; \tOld BW: %d\n", local_new_bw, local_old_bw);

    u16 val = LUT_VAL(local_new_bw);

    // Only if a rwnd value was provided, reduce the window. Otherwise, let the
    // default value be
    if (val != 0) {
      bw_reduced_state = true;
      window_unscaled = (val / RWND_START_REDUCTION_FACTOR);
      if (window_unscaled < MIN_RWND_VAL) {
        window = MIN_RWND_VAL * SCALING;
      } else {
        window = window_unscaled * SCALING;
      }
      curr_max_rwnd = val;

      // Make the step size so large, that the window will grow to the max value
      // in the specified time
      rwnd_per_100_ms =
          (curr_max_rwnd - window_unscaled) / (RWND_GROW_TIME * 10);

      // Make sure that it is never 0 (due to integer division)
      if (rwnd_per_100_ms == 0) {
        rwnd_per_100_ms = 1;
      }

    } else {
      bw_reduced_state = false;
    }

    ktime_get_ts64(&bw_reduced_timer);
    last_rwnd_incr_time = bw_reduced_timer;
  }

  if (bw_reduced_state) {
    struct timespec64 curr_time;
    ktime_get_ts64(&curr_time);

    time64_t delta_s_since_start = curr_time.tv_sec - bw_reduced_timer.tv_sec;

    struct timespec64 delta = timespec64_sub(curr_time, last_rwnd_incr_time);

    // Every ~100ms increase the window slowly, to get a linear growth
    if (delta.tv_nsec > MS_TO_NS(100) || delta.tv_sec > 0) {

      window_unscaled = window_unscaled + rwnd_per_100_ms;
      window_unscaled =
          (window_unscaled > curr_max_rwnd) ? curr_max_rwnd : window_unscaled;

      if (window_unscaled < MIN_RWND_VAL) {
        window = MIN_RWND_VAL * SCALING;
      } else {
        window = window_unscaled * SCALING;
      }

      last_rwnd_incr_time = curr_time;
    }

    if (delta_s_since_start > RWND_HOLD_TIME) {
      bw_reduced_state = false;
    }
  }

  // Set the reserved bits to 101 so that you can see if the ack got adjusted
  // accordingly (Debugging purposes)
  tcphdr->res1 = 0b1010;

  // Reduce recv-window if we are in the propper state
  if (bw_reduced_state) {
    tcphdr->window = htons(window);
  }
#endif

/*
 * Test Functionality for slowly increasing the rwnd
 * This was used to evaluate different rwnd values
 */
#if TEST_RWND_STEPS
  static bool initialized_2 = false;
  static struct timespec64 start_time;
  if (!initialized_2) {
    ktime_get_ts64(&start_time);
    initialized_2 = true;
  }

#define START_RWND 400
  static const u16 stages[] = {START_RWND, 400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                               0,          0,   0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  struct timespec64 curr_time;
  ktime_get_ts64(&curr_time);
  static int ctr = 0;
  static __be16 wnd = htons(START_RWND * SCALING);
  static bool use_default_window = false;
  if (curr_time.tv_sec - start_time.tv_sec > 20) {
    ctr += 1;
    pr_info("20 seconds have passed\n");
    start_time = curr_time;
    use_default_window = (stages[(ctr % 30)] == 0);
    wnd = htons(stages[(ctr % 20)] * SCALING);
  }
  if (!use_default_window) {
    tcphdr->window = wnd;
  }

#endif

  // Do i even need to manually compute the checksum?
  // compute_tcp_checksum(iphdr, (u16 *)tcphdr);

  return NF_ACCEPT;
}

static int listener_thread(void *data) {

  /*
    Format: <OLD_BW(size:3)>|<NEW_BW(size:3)>|
    If a there is no delimiter is pos 3 and 7 something has gone wrong
  */
  const size_t FIRST_DELIM_POS = 3;
  const size_t SECOND_DELIM_POS = 7;

  const size_t buf_size = 48;
  char *buf = (char *)kcalloc(buf_size + 1, sizeof(char), GFP_KERNEL);

  struct kvec iov = {buf, buf_size};
  struct msghdr msg = {.msg_name = NULL, .msg_flags = 0};

  if (IS_ERR_OR_NULL(priv_data->control_sock))
    return 1;

  if (priv_data->control_sock->state != SS_CONNECTED) {
    pr_err("Socket not connected\n");
    return 1;
  }

  while (!kthread_should_stop()) {

    msleep(500);

    // Clear relevant portion of buffer
    memset(buf, 0, 7);

    int num_bytes = kernel_recvmsg(priv_data->control_sock, &msg, &iov, 1,
                                   buf_size, msg.msg_flags);

    pr_info("Received %d bytes (msg: |%s|)\n", num_bytes, buf);

    if (num_bytes < 0) {
      pr_warn("Err Code: %d (%pe)\n", num_bytes, ERR_PTR(num_bytes));
      continue;
    }

    if (buf[FIRST_DELIM_POS] != '|' || buf[SECOND_DELIM_POS] != '|') {
      pr_warn("Delimiter not found in expected positions!\n");
      continue;
    }

    buf[FIRST_DELIM_POS] = '\0';
    int bw = 0;
    int ret = sscanf(buf, "%d", &bw);
    if (ret != 1) {
      pr_warn("Could not convert buf to number (buf: |%s|) (Err: %d; %pe)\n",
              buf, ret, ERR_PTR(ret));
      continue;
    }

    pr_info("Read %d\n", bw);
    atomic_set(&priv_data->new_bw, bw);
  }

  return 0;
}

static void start_listening_thread(void) {
  thread_task = kthread_run(listener_thread, NULL, "listener");
  if (IS_ERR(thread_task)) {
    printk(KERN_ERR "Failed to create listening thread\n");
  }
}

static struct net *net_ns;

static struct nf_hook_ops hook_ops = {
    .hook = custom_hook,
    .pf = NFPROTO_IPV4,
    .hooknum = NF_INET_LOCAL_OUT, //  All packets that were locally generated
                                  //  (e.g. all outgoing packets)
    .priority = 0,                // can be set up to -200?
    .priv = NULL,                 // Can be set to include private data
};

static int custom_hook_init(void) {

  pr_info("Registering custom module NF-HOOK...\n");
  iperf3_port = htons(5201);
  pr_info("Using iperf3 port: %d\n", iperf3_port);

  /* Checkout nsproxy.h -> can be dereferenced without precautions */
  net_ns = current->nsproxy->net_ns;
  if (!net_ns) {
    pr_err("No net_namespace could be found for current\n");
    return -1;
  }

  priv_data = kmalloc(sizeof(struct private_data), GFP_KERNEL);
  if (!priv_data) {
    pr_err("KMalloc Failed!\n");
    return -1;
  }

  // Create socket
  struct socket *sock = kmalloc(sizeof(struct socket), GFP_KERNEL);
  int err =
      sock_create_kern(&init_net, AF_INET, SOCK_STREAM, IPPROTO_TCP, &sock);
  if (err < 0) {
    printk(KERN_ERR "Failed to create socket (%d)\n", err);
    return -1;
  }

  // Connect socket
  {
    struct sockaddr_in *addr = kmalloc(sizeof(struct sockaddr_in), GFP_KERNEL);
    addr->sin_family = AF_INET, addr->sin_port = htons(9998);
    addr->sin_addr = (struct in_addr){.s_addr = htonl(INADDR_LOOPBACK)};
    priv_data->info = addr;
    int ret = kernel_connect(sock, (struct sockaddr *)addr,
                             sizeof(struct sockaddr), O_RDWR);
    if (ret != 0) {
      printk(KERN_ERR "Failed to connect to socket (%d)\n", ret);
      return -1;
    }
  }

  priv_data->control_sock = sock;
  priv_data->new_bw = (atomic_t)ATOMIC_INIT(0);

  hook_ops.priv = (void *)priv_data;

  start_listening_thread();

  return nf_register_net_hook(&init_net, &hook_ops);
}

static void custom_hook_exit(void) {
  pr_info("Unregistering custom module NF-HOOK...\n");
  if (thread_task != NULL) {
    kthread_stop(thread_task);
    msleep(1250);
  }

  priv_data->control_sock->ops->shutdown(priv_data->control_sock, SHUT_RDWR);
  priv_data->control_sock->ops->release(priv_data->control_sock);

  kfree(priv_data->info);
  //   kfree(priv_data->data_sock);
  kfree(priv_data->control_sock);
  kfree(priv_data);

  nf_unregister_net_hook(&init_net, &hook_ops);
}

/* set tcp checksum: given IP header and tcp segment */
// void compute_tcp_checksum(struct iphdr *pIph, u16 *ipPayload) {
//   register unsigned long sum = 0;
//   unsigned short tcpLen = ntohs(pIph->tot_len) - (pIph->ihl << 2);
//   struct tcphdr *tcphdrp = (struct tcphdr *)(ipPayload);
//   // add the pseudo header
//   // the source ip
//   sum += (pIph->saddr >> 16) & 0xFFFF;
//   sum += (pIph->saddr) & 0xFFFF;
//   // the dest ip
//   sum += (pIph->daddr >> 16) & 0xFFFF;
//   sum += (pIph->daddr) & 0xFFFF;
//   // protocol and reserved: 6
//   sum += htons(IPPROTO_TCP);
//   // the length
//   sum += htons(tcpLen);

//   // add the IP payload
//   // initialize checksum to 0
//   tcphdrp->check = 0;
//   while (tcpLen > 1) {
//     sum += *ipPayload++;
//     tcpLen -= 2;
//   }
//   // if any bytes left, pad the bytes and add
//   if (tcpLen > 0) {
//     // printf("+++++++++++padding, %dn", tcpLen);
//     sum += ((*ipPayload) & htons(0xFF00));
//   }
//   // Fold 32-bit sum to 16 bits: add carrier to result
//   while (sum >> 16) {
//     sum = (sum & 0xffff) + (sum >> 16);
//   }
//   sum = ~sum;
//   // set computation result
//   tcphdrp->check = (u16)sum;
// }